package com.example.webdev11.dinosaurdemo;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Movie;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.SystemClock;
import android.util.Log;
import android.view.View;

import java.io.InputStream;

/**
 * Created by webdev11 on 2016-07-26.
 */
public class AugmentedSurface extends View {

    private boolean loaded = false;

    private MainActivity context;

    private enum MovieState {
        Intro,
        Dinosaurs,
        Menu,
        Trex,
        Spinosaurus,
        Brachiosaurus,
        UtahRaptor
    }

    private MovieState state;
    private MediaPlayer mediaPlayer;
    private long mMovieStart;

    private int introGif = R.raw.trex_roar;
    private int introAudio = R.raw.trex_roar_audio;
    private Movie introMovie;

    private int dinosaurGif = R.raw.brachiosaur_walk;
    private int dinosaurFootstep = R.raw.dinosaur_footstep;
    private int dinosaurOverview = R.raw.dinosaur_overview;
    private Movie dinosaurMovie;
    private boolean showDinosaur = true;

    private int meteorGif = R.raw.meteor;
    private int meteorAudio = R.raw.meteor_audio;
    private long meteorStart = 0;
    private float meteorScale = 1f;
    private Movie meteorMovie;
    private int meteorPosition = 150;
    private Bitmap explosion;
    private int explosionWidth = 0;
    private int explosionScale = 50;

    private int trexGif = R.raw.trex_walk;
    private int trexAudio = R.raw.trex;
    private Movie trexMovie;

    private int brachiosaurusPosition = 0;
    private int brachiosaurusAudio = R.raw.brachiosaurus;

    private Movie raptorMovie2;
    private int raptorGif2 = R.raw.twin_raptor_run;
    private int raptorCount = 0;

    private int utahRaptorGif = R.raw.utahraptor_slow_run;
    private int utahRaptorAudio = R.raw.utah_raptor_audio;
    private Movie utahRaptorMovie;

    public AugmentedSurface(MainActivity context) {
        super(context);
        this.context = context;
        explosion = BitmapFactory.decodeResource(getResources(), R.raw.explosion);
        if (this.introMovie == null) {
            new LoadStreamTask().execute();
            new LoadExtraStreamTask().execute();
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!loaded) {
            return;
        }

        switch (this.state) {
            case Intro:
                int rel = (int) (SystemClock.uptimeMillis() - mMovieStart) % introMovie.duration();

                if (this.mediaPlayer.getCurrentPosition() < this.mediaPlayer.getDuration()- 1500) {
                    introMovie.setTime(rel);
                    introMovie.draw(canvas, 0, -120);
                }
                else {
                    introMovie.draw(canvas, 0, -120);
                    this.state = MovieState.Dinosaurs;
                    startAudioPlayer(this.dinosaurOverview, true, 1);
                    mMovieStart = SystemClock.uptimeMillis();
                }
                break;
            case Dinosaurs:

                if (showDinosaur) {
                    int relDinosaur = (int) (SystemClock.uptimeMillis() - mMovieStart);
                    if (relDinosaur < dinosaurMovie.duration()) {
                        dinosaurMovie.setTime(relDinosaur);
                        dinosaurMovie.draw(canvas, brachiosaurusPosition, -100);
                    } else {
                        if (meteorStart == 0) {
                            if (mediaPlayer.getCurrentPosition() > 5000){
                                meteorStart = SystemClock.uptimeMillis();
                                startAudioPlayer(this.meteorAudio, false, 1);
                                meteorScale = 0.1f;
                            }

                        }
                        mMovieStart = SystemClock.uptimeMillis()-2400;
                        rel = (int) (SystemClock.uptimeMillis() - mMovieStart);
                        brachiosaurusPosition -= 41;
                        dinosaurMovie.setTime(rel);
                        dinosaurMovie.draw(canvas, brachiosaurusPosition, -100);
                    }
                }
                if (meteorStart != 0) {
                    meteorScale += 0.003;
                    if (meteorScale > 1) {
                        meteorScale += 0.5;
                        canvas.drawBitmap(Bitmap.createScaledBitmap(explosion, explosionScale, explosionScale, false), meteorPosition - 150 ,meteorPosition + 20, null);
                        meteorPosition -= 2;
                        explosionScale += 10;
                        if (meteorPosition < -90) {
                            this.state = MovieState.Menu;
                            this.meteorPosition = 150;
                            explosionScale = 50;
                            meteorPosition = 150;
                            this.meteorStart = 0;
                            this.meteorScale = 1f;
                            showDinosaur = true;
                            explosionWidth = 0;
                            brachiosaurusPosition = 0;
                            this.context.displayMenu();
                        }
                        else if (meteorPosition < -20) {
                            showDinosaur = false;
                        }

                    } else {
                        int relMeteor = (int) (SystemClock.uptimeMillis() - meteorStart) % meteorMovie.duration();
                        meteorMovie.setTime(relMeteor);
                        meteorMovie.draw(canvas, 2000 - explosionWidth, -30);
                        explosionWidth += 10;
                    }

                }

                break;
            case Menu:
                break;
            case Trex:
                rel = (int) (SystemClock.uptimeMillis() - mMovieStart);

                if (rel < trexMovie.duration()) {
                    trexMovie.setTime(rel);
                    trexMovie.draw(canvas, -80, -120);
                }
                else {
                    this.state = MovieState.Menu;
                    context.displayMenu();
                }
                break;
            case Spinosaurus:
                break;
            case Brachiosaurus:
                rel = (int) (SystemClock.uptimeMillis() - mMovieStart);

                if (rel < dinosaurMovie.duration()) {
                    dinosaurMovie.setTime(rel);
                    dinosaurMovie.draw(canvas, -120, -120);
                }
                else {
                    this.state = MovieState.Menu;
                    context.displayMenu();
                }
                break;
            case UtahRaptor:
                rel = (int) (SystemClock.uptimeMillis() - mMovieStart);
                if (raptorCount < 350) {
                    if (rel < utahRaptorMovie.duration()) {
                        utahRaptorMovie.setTime(rel);
                        utahRaptorMovie.draw(canvas, 0, 0);
                    }
                    if (raptorCount == 349) {
                        mMovieStart = SystemClock.uptimeMillis();
                    }
                } else if (raptorCount < 600) {
                    raptorMovie2.setTime(rel);
                    raptorMovie2.draw(canvas, 20, 0);
                }
                else {
                    this.state = MovieState.Menu;
                    raptorCount = 0;
                    context.displayMenu();
                }
                raptorCount += 1;
                break;
        }
        this.invalidate();
    }

    private void startAudioPlayer(int audio, boolean cancel, float volume) {
        if (cancel && mediaPlayer != null && mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
        try {
            mediaPlayer = MediaPlayer.create(getContext(), audio);
            mediaPlayer.setVolume(volume, volume);
            mediaPlayer.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void backToStart() {
        mMovieStart = SystemClock.uptimeMillis();
        startAudioPlayer(introAudio, true, 1);
        state = MovieState.Intro;
        invalidate();
    }

    public void showTRex() {
        this.mMovieStart = SystemClock.uptimeMillis();
        this.state = MovieState.Trex;
        startAudioPlayer(trexAudio, true, 1);
    }

    public void showBrachiosaurus() {
        this.mMovieStart = SystemClock.uptimeMillis();
        this.state = MovieState.Brachiosaurus;
        startAudioPlayer(brachiosaurusAudio, true, 1);
    }

    public void showRaptor() {
        this.mMovieStart = SystemClock.uptimeMillis();
        this.state = MovieState.UtahRaptor;
        startAudioPlayer(utahRaptorAudio, true, 1);
    }

    private class LoadStreamTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... urls) {

            InputStream is = context.getResources().openRawResource(introGif);
            introMovie = Movie.decodeStream(is);
            is = context.getResources().openRawResource(dinosaurGif);
            dinosaurMovie = Movie.decodeStream(is);
            is = context.getResources().openRawResource(meteorGif);
            meteorMovie = Movie.decodeStream(is);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            mMovieStart = SystemClock.uptimeMillis();
            state = MovieState.Intro;
            //context.displayMenu();
            startAudioPlayer(introAudio, true, 1);
            loaded = true;
            invalidate();
        }

    }

    private class LoadExtraStreamTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... urls) {

            InputStream is = context.getResources().openRawResource(raptorGif2);
            raptorMovie2 = Movie.decodeStream(is);
            is = context.getResources().openRawResource(utahRaptorGif);
            utahRaptorMovie = Movie.decodeStream(is);
            is = context.getResources().openRawResource(trexGif);
            trexMovie = Movie.decodeStream(is);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
        }

    }
}
