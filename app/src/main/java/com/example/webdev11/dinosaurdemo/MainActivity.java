package com.example.webdev11.dinosaurdemo;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;

public class MainActivity extends Activity {

    private AugmentedSurface augmentedSurface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        FrameLayout layout = (FrameLayout) findViewById(R.id.frame_layout);

        //CameraSurface cameraSurface = new CameraSurface(this);
        //if (cameraSurface.isCameraAvailable()) {
          //  layout.addView(cameraSurface);
            augmentedSurface = new AugmentedSurface(this);
            layout.addView(augmentedSurface);

        //} else {
        //    setContentView(R.layout.error_view);
        //}
    }

    @Override public boolean onKeyUp(int keyCode, KeyEvent event)     {
        return false;
    }

    public void displayMenu() {
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.menu, null);
        v.findViewById(R.id.villa_button).requestFocus();
        FrameLayout layout = (FrameLayout) findViewById(R.id.frame_layout);
        layout.addView(v);
        v.startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_to_left));

    }

    public void removeMenu() {
        View options = findViewById(R.id.menu);
        options.startAnimation(AnimationUtils.loadAnimation(this, R.anim.slide_to_right));
        ((ViewGroup) options.getParent()).removeView(options);
    }

    public void backToStart(View view) {
        removeMenu();
        this.augmentedSurface.backToStart();
    }

    public void showRaptor(View view) {
        removeMenu();
        this.augmentedSurface.showRaptor();
    }

    public void showTRex(View view) {
        removeMenu();
        this.augmentedSurface.showTRex();
    }

    public void showBrachiosaurus(View view) {
        removeMenu();
        this.augmentedSurface.showBrachiosaurus();
    }
}
